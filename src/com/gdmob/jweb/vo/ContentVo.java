package com.gdmob.jweb.vo;

public class ContentVo {
  private int id ;
  public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
private String title;
  private String modifytime;
  private String content;
public String getTitle() {
	return title;
}
public void setTitle(String title) {
	this.title = title;
}
public String getModifytime() {
	return modifytime;
}
public void setModifytime(String modifytime) {
	this.modifytime = modifytime;
}
public String getContent() {
	return content;
}
public void setContent(String content) {
	this.content = content;
}
}
