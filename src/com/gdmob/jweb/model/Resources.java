package com.gdmob.jweb.model;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;

@SuppressWarnings("unused")
@Table(tableName="jw_resources")
public class Resources extends BaseModel<Resources> {
	
	private static final long serialVersionUID = 2051998642258015518L;

	private static Logger log = Logger.getLogger(Resources.class);
	
	public static final Resources dao = new Resources();
	
}
