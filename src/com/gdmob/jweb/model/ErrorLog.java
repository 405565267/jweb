package com.gdmob.jweb.model;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Table;

@SuppressWarnings("unused")
@Table(tableName="jw_errorlog")
public class ErrorLog extends BaseModel<ErrorLog> {

	private static final long serialVersionUID = 2592706064072264621L;

	private static Logger log = Logger.getLogger(ErrorLog.class);
	
	public static final ErrorLog dao = new ErrorLog();
	
}
