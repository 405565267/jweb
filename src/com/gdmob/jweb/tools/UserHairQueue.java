package com.gdmob.jweb.tools;
import java.util.concurrent.ArrayBlockingQueue;
import com.gdmob.jweb.vo.UserHairVO;

public class UserHairQueue {
 public static final ArrayBlockingQueue<UserHairVO> queue = new ArrayBlockingQueue<UserHairVO>(50000);
}
