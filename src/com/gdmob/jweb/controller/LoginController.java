package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.model.User;
import com.gdmob.jweb.service.LoginService;
import com.gdmob.jweb.tools.ToolContext;
import com.gdmob.jweb.tools.ToolWeb;
import com.gdmob.jweb.validator.LoginValidator;
import com.jfinal.aop.Before;

/**
 * 登陆处理
 */
@Controller(controllerKey = "/jw/login")
public class LoginController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(LoginController.class);
	
	/**
	 * 准备登陆
	 */
	public void index() {
		User user = ToolContext.getCurrentUser(getRequest(), true); // cookie认证自动登陆处理
		if(null != user){//后台
			redirect("/jw/");
		}
		else
		{
			render("/pingtai/login.html");
		}
	}
	
	/**
	 * 登陆验证
	 */
	@Before(LoginValidator.class)
	public void vali() {
		boolean authCode = authCode();
		
		String username = getPara("username");
		String password = getPara("password");
		String remember = getPara("remember");
		boolean autoLogin = false;
		if(null != remember && remember.equals("1"))
		{
		   autoLogin = true;
		}
		int result = LoginService.service.login(getRequest(), getResponse(), username, password, autoLogin);
		if(result != DictKeys.login_info_3)
		{
			setAttr("msg1","用户名或密码错误！");
			render("/pingtai/login.html");
		    //redirect("/jw/login");	
		    return;
		}
		if(authCode)
		{
			redirect("/jw/index");
		}
		else
		{
			setAttr("msg2","验证码错误！");
			render("/pingtai/login.html");
		    //redirect("/jw/login");	
		}
	}

	/**
	 * 注销
	 */
	public void logout() {
		ToolWeb.addCookie(getResponse(), "", "/", true, "authmark", null, 0);
		redirect("/jw/login");
	}

}
