package com.gdmob.jweb.controller;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.common.DictKeys;
import com.gdmob.jweb.plugin.PropertiesPlugin;
import com.gdmob.jweb.service.UploadService;
import com.gdmob.jweb.tools.ToolString;
import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;

/**
 * 文件上传
 */
@SuppressWarnings("unused")
@Controller(controllerKey = {"/jw/upload"})
public class UploadController extends BaseController {

	private static Logger log = Logger.getLogger(UploadController.class);
	
	/**
	 * 默认是保存到	/WebContent/files/upload
	 * 否则保存到	/WebContent/WEB-INF/files/upload
	 */
	private String pathType;  
	
/*	public void index() {
		StringBuilder sb = new StringBuilder();
		if(null == pathType || pathType.isEmpty() || pathType.equals("webRoot")){
			pathType = "webRoot";
			sb.append(PathKit.getWebRootPath()).append(File.separator).append("files").append(File.separator).append("upload");
		}else{
			pathType = "webInf";
			sb.append(PathKit.getWebRootPath()).append(File.separator).append("WEB-INF").append(File.separator).append("files").append(File.separator).append("upload");
		}
		
		List<UploadFile> files = getFiles(sb.toString(), (Integer) PropertiesPlugin.getParamMapValue(DictKeys.config_maxPostSize_key), ToolString.encoding);
		
		List<Map<String, String>> list = UploadService.service.upload(pathType, files);
		renderJson(list);
	}*/
	
}
