package com.gdmob.jweb.controller;

import org.apache.log4j.Logger;

import com.gdmob.jweb.annotation.Controller;
import com.gdmob.jweb.model.Menu;
import com.gdmob.jweb.service.MenuService;
import com.gdmob.jweb.validator.MenuValidator;
import com.jfinal.aop.Before;

@Controller(controllerKey = "/jw/menu")
public class MenuController extends BaseController {

	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(MenuController.class);
	
	private String pIds;
	private String names;
	private int orderIds;
	private String operatorIds;
	
	public void index() {
		render("/pingtai/menu/tree.html");
	}

	public void treeData()  {
		String jsonText = MenuService.service.childNodeData(ids, getI18nPram());
		renderJson(jsonText);
	}
	
	@Before(MenuValidator.class)
	public void save() {
		ids = MenuService.service.save(pIds, names, orderIds, getI18nPram());
		renderText(ids);
	}
	
	@Before(MenuValidator.class)
	public void update() {
		MenuService.service.update(ids, pIds, names, getI18nPram());
		renderText(ids);
	}
	
	public void delete() {
		MenuService.service.delete(ids);
		renderText(ids);
	}

	public void getOperator(){
		Menu menu = Menu.dao.findById(ids);
		renderJson(menu);
	}

	public void setOperator(){
		MenuService.service.setOperator(ids, operatorIds);
		renderJson(ids);
	}
	
	/**
	 * 准备更新
	 */
	public void toEdit() {
		Menu menu = Menu.dao.findById(ids);
		setAttr("menu", menu);
		render("/pingtai/menu/edit.html");
	}
	
	/**
	 * 更新
	 */
	public void edit() {
		Menu menu = getModel(Menu.class);
		menu.update();
		menu = Menu.dao.findById(menu.getStr("ids"));
		redirect("/jw/menu?systemsIds=" + menu.getStr("systemsids"));
	}
}


